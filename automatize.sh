echo "======= entering directory analsysis"
pushd analysis
echo "======= running root "
# nominal
#root -l MC16Analysis_v7_different_binning.C'("/afs/cern.ch/user/b/bmondal/eos/physics_analysis/tty/efake_study/version_165/processed_ntuple_used_for_fitting/MC16Analysis_v7_nominal_different_binning.root")' 
# nominal loose
root -l -q MC16Analysis_v7_different_binning.C'("/afs/cern.ch/user/b/bmondal/eos/physics_analysis/tty/efake_study/version_165/processed_ntuple_used_for_fitting/MC16Analysis_v7_different_binning_nominalLoose.root")' > log_mc.txt &

#nominal
#root -l DataAnalysis_v7_different_binning_final.C'("/afs/cern.ch/user/b/bmondal/eos/physics_analysis/tty/efake_study/version_165/output-MC16Analysis_v7/DataAnalysis_v7_different_binning_final.root")'
#nominal loose
root -l -q DataAnalysis_v7_different_binning_final.C'("/afs/cern.ch/user/b/bmondal/eos/physics_analysis/tty/efake_study/version_165/processed_ntuple_used_for_fitting/DataAnalysis_v7_different_binning_nominalLoose.root")' > log_data.txt &
popd
