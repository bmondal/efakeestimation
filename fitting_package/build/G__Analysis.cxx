// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME G__Analysis
#define R__NO_DEPRECATION

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// The generated code does not explicitly qualifies STL entities
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "RooMyPDF_DSCB.h"
#include "Model.h"
#include "/afs/cern.ch/user/b/bmondal/work/atlasrootstyle/AtlasStyle.h"
#include "/afs/cern.ch/user/b/bmondal/work/atlasrootstyle/AtlasLabels.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static void *new_RooMyPDF_DSCB(void *p = 0);
   static void *newArray_RooMyPDF_DSCB(Long_t size, void *p);
   static void delete_RooMyPDF_DSCB(void *p);
   static void deleteArray_RooMyPDF_DSCB(void *p);
   static void destruct_RooMyPDF_DSCB(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RooMyPDF_DSCB*)
   {
      ::RooMyPDF_DSCB *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::RooMyPDF_DSCB >(0);
      static ::ROOT::TGenericClassInfo 
         instance("RooMyPDF_DSCB", ::RooMyPDF_DSCB::Class_Version(), "RooMyPDF_DSCB.h", 16,
                  typeid(::RooMyPDF_DSCB), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::RooMyPDF_DSCB::Dictionary, isa_proxy, 4,
                  sizeof(::RooMyPDF_DSCB) );
      instance.SetNew(&new_RooMyPDF_DSCB);
      instance.SetNewArray(&newArray_RooMyPDF_DSCB);
      instance.SetDelete(&delete_RooMyPDF_DSCB);
      instance.SetDeleteArray(&deleteArray_RooMyPDF_DSCB);
      instance.SetDestructor(&destruct_RooMyPDF_DSCB);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RooMyPDF_DSCB*)
   {
      return GenerateInitInstanceLocal((::RooMyPDF_DSCB*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::RooMyPDF_DSCB*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_Model(void *p = 0);
   static void *newArray_Model(Long_t size, void *p);
   static void delete_Model(void *p);
   static void deleteArray_Model(void *p);
   static void destruct_Model(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Model*)
   {
      ::Model *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Model >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Model", ::Model::Class_Version(), "Model.h", 17,
                  typeid(::Model), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Model::Dictionary, isa_proxy, 4,
                  sizeof(::Model) );
      instance.SetNew(&new_Model);
      instance.SetNewArray(&newArray_Model);
      instance.SetDelete(&delete_Model);
      instance.SetDeleteArray(&deleteArray_Model);
      instance.SetDestructor(&destruct_Model);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Model*)
   {
      return GenerateInitInstanceLocal((::Model*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Model*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

//______________________________________________________________________________
atomic_TClass_ptr RooMyPDF_DSCB::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *RooMyPDF_DSCB::Class_Name()
{
   return "RooMyPDF_DSCB";
}

//______________________________________________________________________________
const char *RooMyPDF_DSCB::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooMyPDF_DSCB*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int RooMyPDF_DSCB::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooMyPDF_DSCB*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *RooMyPDF_DSCB::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooMyPDF_DSCB*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *RooMyPDF_DSCB::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooMyPDF_DSCB*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr Model::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *Model::Class_Name()
{
   return "Model";
}

//______________________________________________________________________________
const char *Model::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Model*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int Model::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Model*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *Model::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Model*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *Model::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Model*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
void RooMyPDF_DSCB::Streamer(TBuffer &R__b)
{
   // Stream an object of class RooMyPDF_DSCB.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(RooMyPDF_DSCB::Class(),this);
   } else {
      R__b.WriteClassBuffer(RooMyPDF_DSCB::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_RooMyPDF_DSCB(void *p) {
      return  p ? new(p) ::RooMyPDF_DSCB : new ::RooMyPDF_DSCB;
   }
   static void *newArray_RooMyPDF_DSCB(Long_t nElements, void *p) {
      return p ? new(p) ::RooMyPDF_DSCB[nElements] : new ::RooMyPDF_DSCB[nElements];
   }
   // Wrapper around operator delete
   static void delete_RooMyPDF_DSCB(void *p) {
      delete ((::RooMyPDF_DSCB*)p);
   }
   static void deleteArray_RooMyPDF_DSCB(void *p) {
      delete [] ((::RooMyPDF_DSCB*)p);
   }
   static void destruct_RooMyPDF_DSCB(void *p) {
      typedef ::RooMyPDF_DSCB current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RooMyPDF_DSCB

//______________________________________________________________________________
void Model::Streamer(TBuffer &R__b)
{
   // Stream an object of class Model.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Model::Class(),this);
   } else {
      R__b.WriteClassBuffer(Model::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_Model(void *p) {
      return  p ? new(p) ::Model : new ::Model;
   }
   static void *newArray_Model(Long_t nElements, void *p) {
      return p ? new(p) ::Model[nElements] : new ::Model[nElements];
   }
   // Wrapper around operator delete
   static void delete_Model(void *p) {
      delete ((::Model*)p);
   }
   static void deleteArray_Model(void *p) {
      delete [] ((::Model*)p);
   }
   static void destruct_Model(void *p) {
      typedef ::Model current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Model

namespace {
  void TriggerDictionaryInitialization_libAnalysis_Impl() {
    static const char* headers[] = {
"RooMyPDF_DSCB.h",
"Model.h",
"/afs/cern.ch/user/b/bmondal/work/atlasrootstyle/AtlasStyle.h",
"/afs/cern.ch/user/b/bmondal/work/atlasrootstyle/AtlasLabels.h",
0
    };
    static const char* includePaths[] = {
"/cvmfs/sft.cern.ch/lcg/releases/LCG_97a/ROOT/v6.20.06/x86_64-centos7-gcc8-opt/include",
"/afs/cern.ch/user/b/bmondal/work/ttgamma-analysis/efake_calculation_18082021/efakeestimation/fitting_package/source",
"/cvmfs/sft.cern.ch/lcg/releases/ROOT/v6.20.06-3f7fd/x86_64-centos7-gcc8-opt/include/",
"/afs/cern.ch/work/b/bmondal/ttgamma-analysis/efake_calculation_18082021/efakeestimation/fitting_package/build/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "libAnalysis dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
class __attribute__((annotate(R"ATTRDUMP(Your description goes here...)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$RooMyPDF_DSCB.h")))  RooMyPDF_DSCB;
class __attribute__((annotate("$clingAutoload$Model.h")))  Model;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "libAnalysis dictionary payload"


#define _BACKWARD_BACKWARD_WARNING_H
// Inline headers
#include "RooMyPDF_DSCB.h"
#include "Model.h"
#include "/afs/cern.ch/user/b/bmondal/work/atlasrootstyle/AtlasStyle.h"
#include "/afs/cern.ch/user/b/bmondal/work/atlasrootstyle/AtlasLabels.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[] = {
"Model", payloadCode, "@",
"RooMyPDF_DSCB", payloadCode, "@",
nullptr
};
    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("libAnalysis",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_libAnalysis_Impl, {}, classesHeaders, /*hasCxxModule*/false);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_libAnalysis_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_libAnalysis() {
  TriggerDictionaryInitialization_libAnalysis_Impl();
}
