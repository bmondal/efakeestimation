cp ../run/converted/nominal/converted_nominal__sf.root .
cp ../run/converted/syst_bkg_var/converted_syst_bkg_sf.root .
cp ../run/converted/syst_mass_var/converted_syst_mass_sf.root .
cp ../run/converted/syst_sig_var/h2_sf_nominal.root converted_syst_sig_sf.root

cp ../run/unconverted/nominal/unconverted_nominal__sf.root .
cp ../run/unconverted/syst_bkg_var/unconverted_syst_bkg_sf.root .
cp ../run/unconverted/syst_mass_var/unconverted_syst_mass_sf.root .
cp ../run/unconverted/syst_sig_var/h2_sf_nominal.root unconverted_syst_sig_sf.root
