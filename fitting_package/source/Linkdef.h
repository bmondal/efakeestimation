#ifdef __CINT__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;

// Common
#pragma link C++ class RooMyPDF_DSCB+;
#pragma link C++ class Model+;

#endif
