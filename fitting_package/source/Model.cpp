//
// Created by bmondal on 07/07/21.
//

#include <RooRealVar.h>
#include <RooDataHist.h>
#include <RooBernstein.h>
#include <TCanvas.h>
#include "Model.h"
#include "RooMyPDF_DSCB.h"

#include "RooFitResult.h"
#include "RooConstVar.h"
#include "RooAddPdf.h"
#include "RooWorkspace.h"
#include "RooPlot.h"
#include "RooPlotable.h"
#include "TFile.h"
#include "TH1.h"
#include "TLegend.h"
#include "TStyle.h"

#include "/afs/cern.ch/user/b/bmondal/work/atlasrootstyle/AtlasStyle.h"
#include "/afs/cern.ch/user/b/bmondal/work/atlasrootstyle/AtlasLabels.h"

ClassImp(Model);

Model::Model(TH1F *hist, double invMassMin, double invMassMax) {
  h1 = hist;
  m_invMassMin = invMassMin;
  m_invMassMax = invMassMax;
  setMean( 90, 80, 100);
  setSigma(5.0,0.,10);
  setNL(1.0,0.0, 10.0);
  setNR(10.0,0.0, 50.0);
  setAlpaL(1,0, 10.0);
  setAlpaR(2,0, 10.0);
  setBernsteinBkg( 0.5, 0., 10., 0.3, 0., 10., 0.2, 0., 10., 0.1, 0., 10.);
}

void Model::makeModel_DSCB_Bern() {
  x = new RooRealVar("x", "Invariant Mass (GeV)", m_invMassMin, m_invMassMax);
  data = new RooDataHist("data", "dataset with x", *x, h1);
  mean = RooRealVar("mean1", "mean of gaussians1", m_meanParam1, m_meanParam2, m_meanParam3);
  sigma = RooRealVar("sigma1", "width of gaussians", m_sigmaParam1, m_sigmaParam2, m_sigmaParam3);
  nL = RooRealVar("nL","nL", m_nLParam1, m_nLParam2, m_nLParam3);
  nR = RooRealVar("nR","nR", m_nRParam1, m_nRParam2, m_nRParam3);
  alphaL = RooRealVar("alphaL","alphaL", m_alphaLParam1, m_alphaLParam2, m_alphaLParam3);
  alphaR = RooRealVar("alphaR","alphaR", m_alphaRParam1, m_alphaRParam2, m_alphaRParam3);
  a0 = RooRealVar("a0", "a0", m_a0Param1, m_a0Param2, m_a0Param3);
  a1 = RooRealVar("a1", "a1", m_a1Param1, m_a1Param2, m_a1Param3);
  a2 = RooRealVar("a2", "a2", m_a2Param1, m_a2Param2, m_a2Param3);
  a3 = RooRealVar("a3", "a3", m_a3Param1, m_a3Param2, m_a3Param3);

  sig = new RooMyPDF_DSCB("sig", "Signal component ", *x, mean, sigma, alphaL, nL, alphaR, nR);
  bkg_bern = new RooBernstein("bkg","Background",*x,RooArgSet(a0, a1, a2, a3));
  // yields
  n_sig = new RooRealVar("n_sig", "nsig", h1->Integral()*0.9, 0, h1->Integral());
  n_bkg = new RooRealVar("n_bkg", "nbkg", h1->Integral()*0.1, 0, h1->Integral());

  // complete fit model
  model = new RooAddPdf("model", "signal+background", RooArgList(*bkg_bern, *sig), RooArgList(*n_bkg, *n_sig));
}

void Model::makeModel_DSCB_Gaus() {
  x = new RooRealVar("x", "Invariant Mass (GeV)", m_invMassMin, m_invMassMax);
  data = new RooDataHist("data", "dataset with x", *x, h1);
  mean = RooRealVar("mean1", "mean of gaussians1", m_meanParam1, m_meanParam2, m_meanParam3);
  sigma = RooRealVar("sigma1", "width of gaussians", m_sigmaParam1, m_sigmaParam2, m_sigmaParam3);
  nL = RooRealVar("nL","nL", m_nLParam1, m_nLParam2, m_nLParam3);
  nR = RooRealVar("nR","nR", m_nRParam1, m_nRParam2, m_nRParam3);
  alphaL = RooRealVar("alphaL","alphaL", m_alphaLParam1, m_alphaLParam2, m_alphaLParam3);
  alphaR = RooRealVar("alphaR","alphaR", m_alphaRParam1, m_alphaRParam2, m_alphaRParam3);
  mean_gaus = RooRealVar("mean", "mean of gaussians1", m_meanGausParam1, m_meanGausParam2, m_meanGausParam3);
  sigma_gaus = RooRealVar("sigma", "width of gaussians", m_sigmaGausParam1, m_sigmaGausParam2, m_sigmaGausParam3);

  sig = new RooMyPDF_DSCB("sig", "Signal component ", *x, mean, sigma, alphaL, nL, alphaR, nR);
  bkg_gaus = new RooGaussian("bkg", "background", *x, mean_gaus, sigma_gaus);
  // yields
  n_sig = new RooRealVar("n_sig", "nsig", h1->Integral()*0.9, 0, h1->Integral());
  n_bkg = new RooRealVar("n_bkg", "nbkg", h1->Integral()*0.1, 0, h1->Integral());

  // complete fit model
  model = new RooAddPdf("model", "signal+background", RooArgList(*bkg_gaus, *sig), RooArgList(*n_bkg, *n_sig));
}

void Model::doFitting() {
  model->fitTo(*data);
}



void Model::plotFit(int& EtaBin, int& PtBin, TString whichFit) {
  //set atlas style
  SetAtlasStyle();
  RooPlot* frame = x->frame(RooFit::Title(" "));
  // to calculate the correct chi2 the first plot should be data and last plot should be the full model
  data->plotOn(frame);
  model->plotOn(frame, RooFit::Components(*sig), RooFit::LineColor(kRed), RooFit::Name("signal"));
  if(!isBkgGaus)model->plotOn(frame, RooFit::Components(*bkg_bern), RooFit::LineColor(kGreen), RooFit::Name("bkg"));
  if(isBkgGaus)model->plotOn(frame, RooFit::Components(*bkg_gaus), RooFit::LineColor(kGreen), RooFit::Name("bkg"));
  model->plotOn(frame,RooFit::Name("sig+bkg"));
  ///
  auto *c1 = new TCanvas("c1", "c1", 1200, 1200);
  gPad->SetLeftMargin(0.15);
  frame->GetYaxis()->SetTitleOffset(1.4);
  frame->GetYaxis()->SetTitle("Events / 0.5 Gev");
  frame->Draw("E1HIST");
  gStyle->SetOptStat();
  //auto* leg0 = new TLegend(0.15, 0.9, 0.45, 0.95);
  //leg0->Draw("same");
  auto* leg1 = new TLegend(0.15, 0.8, 0.45, 0.88);
  leg1->SetFillColor(kWhite);
  leg1->SetLineColor(kBlack);
  //TString fitName = whichFit + Form("_EtaBin%d_PtBin%d",EtaBin+1,PtBin+1);
  TString fitName = Form("Postfit_Zpeak_PtBin%d_EtaBin%d_",PtBin+1,EtaBin+1)+ whichFit;
  // for getting pt and eta information
  std::map<int, std::string> map_pt;
  map_pt[1] = "[20-35]";
  map_pt[2] = "[35-45]";
  map_pt[3] =  "[45-1000]";
  std::map <int, std::string> map_eta;
  map_eta[1] = "[0.0-0.6]";
  map_eta[2] = "[0.6-1.37]";
  map_eta[3] = "[1.37-1.52]";
  map_eta[4] = "[1.52-1.8]";
  map_eta[5] = "[1.8-2.37]";
  TString legFigName;
  legFigName = whichFit + ";  Pt"+ map_pt.at(PtBin + 1)+",|Eta|"+map_eta.at(EtaBin + 1);
  leg1->AddEntry("", legFigName, "");
  //leg1->AddEntry("", "ATLAS Internal","" );
  ATLASLabel(0.2,0.7,"Internal");
  leg1->Draw("same");
  auto* leg2 = new TLegend(0.7, 0.7, 0.88, 0.88);
  leg2->SetFillColor(kWhite);
  leg2->SetLineColor(kBlack);
  leg2->AddEntry("data","Data", "EP");
  leg2->AddEntry("sig+bkg","Sig+Bkg","LP");
  leg2->AddEntry("bkg","Bkg", "LP");
  leg2->AddEntry("signal","Sig", "LP");
  leg2->AddEntry("frame->chiSquare()",Form("#chi^{2}/ndf= %.2f",frame->chiSquare()),"");
  leg2->Draw("same");
  //c1 -> SaveAs(Form("Zeg_fit_eta%dpt%d.png",EtaBin+1,PtBin+1));
  c1 -> SaveAs(fitName+".pdf");
  ///
  auto *c2 = new TCanvas("c2", "c2", 1200, 1200);
  c2->SetLogy();
  gPad->SetLeftMargin(0.15);
  frame->GetYaxis()->SetTitleOffset(1.4);
  frame->Draw("E1HIST");
  gStyle->SetOptStat();
  leg1->Draw("same");
  leg2->Draw("same");
  //c2 -> SaveAs(Form("Zeg_fit_eta%dpt%d_1.png",EtaBin+1,PtBin+1));
  c2 -> SaveAs(fitName+"_1.pdf");
  //params->Print();
  //result->Print();
  RooPlot* frame1 = x->frame(RooFit::Title(" "));
  data->plotOn(frame1);
  model->plotOn(frame1,RooFit::Name("sig+bkg"));
  // C a l c u l a t e   c h i ^ 2
  std::cout << "chi^2 = " << frame->chiSquare() << std::endl ;
  // S h o w   r e s i d u a l   a n d   p u l l   d i s t s
  // -------------------------------------------------------
  // Construct a histogram with the residuals of the data w.r.t. the curve
  RooHist* hresid = frame->residHist() ;
  // Construct a histogram with the pulls of the data w.r.t the curve
  RooHist* hpull = frame->pullHist() ;
  // Create a new frame to draw the residual distribution and add the distribution to the frame
  RooPlot* frame2 = x->frame(RooFit::Title("Residual Distribution")) ;
  frame2->addPlotable((RooPlotable*)hresid,"P") ;
  // Create a new frame to draw the pull distribution and add the distribution to the frame
  RooPlot* frame3 = x->frame(RooFit::Title("Pull Distribution")) ;
  frame3->addPlotable((RooPlotable*)hpull,"P") ;
  auto* c = new TCanvas("rf109_chi2residpull","rf109_chi2residpull",1600,800) ;
  c->Divide(3) ;
  c->cd(1) ; gPad->SetLeftMargin(0.15) ; frame1->GetYaxis()->SetTitleOffset(1.6) ; frame1->Draw() ;
  c->cd(2) ; gPad->SetLeftMargin(0.15) ; frame2->GetYaxis()->SetTitleOffset(1.6) ; frame2->Draw() ;
  c->cd(3) ; gPad->SetLeftMargin(0.15) ; frame3->GetYaxis()->SetTitleOffset(1.6) ; frame3->Draw() ;
}

void Model::setMean(double meanParam1, double meanParam2, double meanParam3) {
  m_meanParam1 = meanParam1;
  m_meanParam2 = meanParam2;
  m_meanParam3 = meanParam3;
}

void Model::setSigma(double sigParam1, double sigParam2, double sigParam3) {
  m_sigmaParam1 = sigParam1;
  m_sigmaParam2 = sigParam2;
  m_sigmaParam3 = sigParam3;
}

void Model::setNL(double nLParam1, double nLParam2, double nLParam3) {
  m_nLParam1 = nLParam1;
  m_nLParam2 = nLParam2;
  m_nLParam3 = nLParam3;
}

void Model::setNR(double nRParam1, double nRParam2, double nRParam3) {
  m_nRParam1 = nRParam1;
  m_nRParam2 = nRParam2;
  m_nRParam3 = nRParam3;
}

void Model::setAlpaL(double alphaLParam1, double alphaLParam2, double alphaLParam3) {
  m_alphaLParam1 = alphaLParam1;
  m_alphaLParam2 = alphaLParam2;
  m_alphaLParam3 = alphaLParam3;
}

void Model::setAlpaR(double alphaRParam1, double alphaRParam2, double alphaRParam3) {
  m_alphaRParam1 = alphaRParam1;
  m_alphaRParam2 = alphaRParam2;
  m_alphaRParam3 = alphaRParam3;
}

void
Model::setBernsteinBkg(double a01, double a02, double a03, double a11, double a12, double a13, double a21, double a22,
                       double a23, double a31, double a32, double a33) {
  m_a0Param1 = a01;
  m_a0Param2 = a02;
  m_a0Param3 = a03;
  m_a1Param1 = a11;
  m_a1Param2 = a12;
  m_a1Param3 = a13;
  m_a2Param1 = a21;
  m_a2Param2 = a22;
  m_a2Param3 = a23;
  m_a3Param1 = a31;
  m_a3Param2 = a32;
  m_a3Param3 = a33;
}

Model::~Model(){
  delete x;
  delete data;
  delete sig;
  delete bkg_bern;
  delete n_sig;
  delete n_bkg;
  delete model;
  delete bkg_gaus;
}

void Model::setMeanGaus(double meanGausParam1, double meanGausParam2, double meanGausParam3) {
  m_meanGausParam1 = meanGausParam1;
  m_meanGausParam2 = meanGausParam2;
  m_meanGausParam3 = meanGausParam3;
}

void Model::setSigmaGaus(double sigGausParam1, double sigGausParam2, double sigGausParam3) {
  m_sigmaGausParam1 = sigGausParam1;
  m_sigmaGausParam2 = sigGausParam2;
  m_sigmaGausParam3 = sigGausParam3;
}
