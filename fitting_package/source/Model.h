//
// Created by bmondal on 07/07/21.
//

#ifndef FITTINGEFAKE_MODEL_H
#define FITTINGEFAKE_MODEL_H

#include "TH1F.h"
#include "RooAddPdf.h"
#include "RooMyPDF_DSCB.h"
#include <RooRealVar.h>
#include <RooDataHist.h>
#include <RooBernstein.h>
#include <RooGaussian.h>
#include "RooMyPDF_DSCB.h"

class Model{
public:
    Model() = default;
    Model(TH1F* hist, double invMassMin, double invMassMax); // default constructor
    ~Model(); // default destructor
    void makeModel_DSCB_Bern(); // make model. this sets "model" member function
    void makeModel_DSCB_Gaus(); // make model. this sets "model" member function
    //void setModel(); // this function will help to set RooAddPdf *model to some model. for example DSCB+Bern, DSCB+Gauss, ...
    void doFitting();
    void plotFit(int& EtaBin, int& PtBin, TString whichFit); // whichFit variable only use "Zee" or "Zeg"
    void setMean(double meanParam1, double meanParam2, double meanParam3);
    void setSigma(double sigParam1, double sigParam2, double sigParam3);
    void setMeanGaus(double meanGausParam1, double meanGausParam2, double meanGausParam3);
    void setSigmaGaus(double sigGausParam1, double sigGausParam2, double sigGausParam3);
    void setNL(double nLParam1, double nLParam2, double nLParam3);
    void setNR(double nRParam1, double nRParam2, double nRParam3);
    void setAlpaL(double alphaLParam1, double alphaLParam2, double alphaLParam3);
    void setAlpaR(double alphaRParam1, double alphaRParam2, double alphaRParam3);
    void setBernsteinBkg(double a01, double a02, double a03, double a11, double a12, double a13, double a21, double a22, double a23, double a31, double a32,double  a33);
public:
    bool isBkgGaus = false;
    TH1F* h1 = nullptr; // this is the histogram we will be fitting
    double m_invMassMin;  // this is the min invmass range we will be fitting
    double m_invMassMax;  // this is the max invmass range we will be fitting
    RooRealVar *x = nullptr;
    RooDataHist *data = nullptr;
    RooMyPDF_DSCB *sig = nullptr;
    RooBernstein *bkg_bern = nullptr;
    RooGaussian *bkg_gaus = nullptr;
    RooRealVar *n_sig = nullptr;
    RooRealVar *n_bkg = nullptr;
    RooAddPdf *model = nullptr;
    RooRealVar mean;
    RooRealVar sigma;
    RooRealVar mean_gaus;
    RooRealVar sigma_gaus;
    RooRealVar nL;
    RooRealVar nR;
    RooRealVar alphaL;
    RooRealVar alphaR;
    RooRealVar a0;
    RooRealVar a1;
    RooRealVar a2;
    RooRealVar a3;
    /*
     * fit parameters
     */
    double m_meanParam1;
    double m_meanParam2;
    double m_meanParam3;
    double m_sigmaParam1;
    double m_sigmaParam2;
    double m_sigmaParam3;
    double m_meanGausParam1;
    double m_meanGausParam2;
    double m_meanGausParam3;
    double m_sigmaGausParam1;
    double m_sigmaGausParam2;
    double m_sigmaGausParam3;
    double m_nLParam1;
    double m_nLParam2;
    double m_nLParam3;
    double m_nRParam1;
    double m_nRParam2;
    double m_nRParam3;
    double m_alphaLParam1;
    double m_alphaLParam2;
    double m_alphaLParam3;
    double m_alphaRParam1;
    double m_alphaRParam2;
    double m_alphaRParam3;
    double m_a0Param1;
    double m_a0Param2;
    double m_a0Param3;
    double m_a1Param1;
    double m_a1Param2;
    double m_a1Param3;
    double m_a2Param1;
    double m_a2Param2;
    double m_a2Param3;
    double m_a3Param1;
    double m_a3Param2;
    double m_a3Param3;

private:
ClassDef(Model,1);
};


#endif //FITTINGEFAKE_MODEL_H
