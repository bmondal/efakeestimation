#include <utility>
#include "Model.h"
#include "RooFitResult.h"
#include "RooWorkspace.h"
#include "RooPlot.h"
#include "RooPlotable.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TH1.h"
#include "TLegend.h"
#include "TStyle.h"
#include "TH2F.h"

int low_range[]={60,70,75,80};
//int low_range[]={80,80,80,80};
int high_range[]={120,120,120,120};
double Zee_Sig[5][4]={0};
double Zee_Err[5][4]={0};
double Zeg_Sig[5][4]={0};
double Zeg_Err[5][4]={0};

TString output_prefix = "";

void fit(int& EtaBin, int& PtBin, Model &modelobj, TString whichFit){
	  modelobj.doFitting();
    double nZ = modelobj.n_sig->getVal();
    double eZ = modelobj.n_sig->getError();
    if(whichFit == "zeg") {
      Zeg_Sig[EtaBin][PtBin] = nZ;
      Zeg_Err[EtaBin][PtBin] = eZ;
    }
    if(whichFit == "zee") {
      Zee_Sig[EtaBin][PtBin] = nZ;
      Zee_Err[EtaBin][PtBin] = eZ;
    }
    modelobj.plotFit(EtaBin, PtBin, std::move(whichFit));
}

void calculateFR_SF(TH2F* h2_mc_fakerate){
  gStyle->SetPalette(87);
  const Int_t NBINS_ETA = 5;
  Double_t edges_eta[NBINS_ETA + 1] = {0.0, 0.6, 1.37, 1.52, 1.8, 2.37};
  const Int_t NBINS_PT = 3;
  Double_t edges_pt[NBINS_PT + 1] = {20, 35, 45, 1000};
  TH2F *h2_zee_signal     = new TH2F("h2_zee_signal","h2_zee_signal", NBINS_ETA, edges_eta, NBINS_PT, edges_pt);
  TH2F *h2_zeg_signal     = new TH2F("h2_zeg_signal","h2_zeg_signal", NBINS_ETA, edges_eta, NBINS_PT, edges_pt);

  for(int EtaBin=0;EtaBin<2;EtaBin++){
    for(int PtBin=0;PtBin<3;PtBin++){
      std::cout << "Zee" << EtaBin+1 << PtBin+1 <<"  "<< Zee_Sig[EtaBin][PtBin] << " " << Zee_Err[EtaBin][PtBin] << std::endl;
      std::cout << "Zeg" << EtaBin+1 << PtBin+1 <<"  "<< Zeg_Sig[EtaBin][PtBin] << " " << Zeg_Err[EtaBin][PtBin] << std::endl;
      h2_zee_signal    ->SetBinContent (EtaBin+1,PtBin+1, Zee_Sig[EtaBin][PtBin]);
      h2_zee_signal    ->SetBinError   (EtaBin+1,PtBin+1, Zee_Err[EtaBin][PtBin]);
      h2_zeg_signal    ->SetBinContent (EtaBin+1,PtBin+1, Zeg_Sig[EtaBin][PtBin]);
      h2_zeg_signal    ->SetBinError   (EtaBin+1,PtBin+1, Zeg_Err[EtaBin][PtBin]);
    }
  }
/*  for(int EtaBin=1;EtaBin<2;EtaBin++){
    for(int PtBin=0;PtBin<3;PtBin++){
      std::cout << "Zee" << EtaBin+1 << PtBin+1 <<"  "<< Zee_Sig[EtaBin+1][PtBin] << " " << Zee_Err[EtaBin+1][PtBin] << std::endl;
      std::cout << "Zeg" << EtaBin+1 << PtBin+1 <<"  "<< Zeg_Sig[EtaBin+1][PtBin] << " " << Zeg_Err[EtaBin+1][PtBin] << std::endl;
      h2_zee_signal    ->SetBinContent (EtaBin+1,PtBin+1, Zee_Sig[EtaBin+1][PtBin]);
      h2_zee_signal    ->SetBinError   (EtaBin+1,PtBin+1, Zee_Err[EtaBin+1][PtBin]);
      h2_zeg_signal    ->SetBinContent (EtaBin+1,PtBin+1, Zeg_Sig[EtaBin+1][PtBin]);
      h2_zeg_signal    ->SetBinError   (EtaBin+1,PtBin+1, Zeg_Err[EtaBin+1][PtBin]);
    }
  }*/
  for(int EtaBin=2;EtaBin<4;EtaBin++){
    for(int PtBin=0;PtBin<3;PtBin++){
      std::cout << "Zee" << EtaBin+1 << PtBin+1 <<"  "<< Zee_Sig[EtaBin+1][PtBin] << " " << Zee_Err[EtaBin][PtBin] << std::endl;
      std::cout << "Zeg" << EtaBin+1 << PtBin+1 <<"  "<< Zeg_Sig[EtaBin+1][PtBin] << " " << Zeg_Err[EtaBin][PtBin] << std::endl;
      h2_zee_signal    ->SetBinContent (EtaBin+2,PtBin+1, Zee_Sig[EtaBin+1][PtBin]);
      h2_zee_signal    ->SetBinError   (EtaBin+2,PtBin+1, Zee_Err[EtaBin+1][PtBin]);
      h2_zeg_signal    ->SetBinContent (EtaBin+2,PtBin+1, Zeg_Sig[EtaBin+1][PtBin]);
      h2_zeg_signal    ->SetBinError   (EtaBin+2,PtBin+1, Zeg_Err[EtaBin+1][PtBin]);
    }
  }
  // for(EtaBin=4){
  //   for(PtBin=0;PtBin<4;PtBin++){
  //     h2_zee_signal    ->SetBinContent (EtaBin+1,PtBin+1, Zee_Sig[EtaBin][PtBin]);
  //     h2_zee_signal    ->SetBinError   (EtaBin+1,PtBin+1, Zee_Err[EtaBin][PtBin]);
  //     h2_zeg_signal    ->SetBinContent (EtaBin+1,PtBin+1, Zeg_Sig[EtaBin][PtBin]);
  //     h2_zeg_signal    ->SetBinError   (EtaBin+1,PtBin+1, Zeg_Err[EtaBin][PtBin]);
  //   }
  // }
  TCanvas *c10= new TCanvas("c10","c10",1200,1200);
  h2_zee_signal->Draw("colztexterror");
  c10 -> SaveAs(output_prefix+"h2_zee_signal.png");
  TCanvas *c11= new TCanvas("c11","c11",1200,1200);
  h2_zeg_signal->Draw("colztexterror");
  c11 -> SaveAs(output_prefix+"h2_zeg_signal.png");
  TH2F *h2_fakerate     = new TH2F("h2_fakerate","h2_fakerate", NBINS_ETA, edges_eta, NBINS_PT, edges_pt);
  h2_fakerate->Sumw2();
  h2_fakerate->Divide(h2_zeg_signal,h2_zee_signal);
  TCanvas *c13= new TCanvas("c13","13",1200,1200);
  h2_fakerate->SetTitle("2D data fake rate");
  h2_fakerate->GetXaxis()->SetTitle("|eta|");
  h2_fakerate->GetYaxis()->SetTitle("pT [GeV]");
  h2_fakerate->GetXaxis()->SetBinLabel(1,"[0.0 - 0.6]");
  h2_fakerate->GetXaxis()->SetBinLabel(2,"[0.6 - 1.37]");
  h2_fakerate->GetXaxis()->SetBinLabel(3,"[1.37 - 1.52]");
  h2_fakerate->GetXaxis()->SetBinLabel(4,"[1.52 - 1.8]");
  h2_fakerate->GetXaxis()->SetBinLabel(5,"[1.8 - 2.37]");
  h2_fakerate->GetYaxis()->SetBinLabel(1,"[20 - 35] ");
  h2_fakerate->GetYaxis()->SetBinLabel(2,"[35 - 45] ");
  h2_fakerate->GetYaxis()->SetBinLabel(3,"[45 - 1000] ");
  //h2_fakerate->GetYaxis()->SetBinLabel(4,"[60 - 100]");
  h2_fakerate->Scale(100);
  h2_fakerate->SetStats(0);
  h2_fakerate->Draw("colztexterror");
  gStyle->SetPaintTextFormat("4.2f");
  h2_fakerate->SaveAs(output_prefix+"h2_data_FR.root");
  c13->SetLogy();
  c13 -> SaveAs(output_prefix+"h2_data_fc.png");
  c13 -> SaveAs(output_prefix+"h2_data_fc.svg");
  TCanvas *c14= new TCanvas("c14","c14",1200,1200);
  h2_mc_fakerate->SetTitle("2D MC fake rate");
  h2_mc_fakerate->GetXaxis()->SetTitle("|eta|");
  h2_mc_fakerate->GetYaxis()->SetTitle("pT [GeV]");
  h2_mc_fakerate->GetXaxis()->SetBinLabel(1,"[0.0 - 0.6]");
  h2_mc_fakerate->GetXaxis()->SetBinLabel(2,"[0.6 - 1.37]");
  h2_mc_fakerate->GetXaxis()->SetBinLabel(3,"[1.37 - 1.52]");
  h2_mc_fakerate->GetXaxis()->SetBinLabel(4,"[1.52 - 1.8]");
  h2_mc_fakerate->GetXaxis()->SetBinLabel(5,"[1.8 - 2.37]");
  h2_mc_fakerate->GetYaxis()->SetBinLabel(1,"[20 - 35] ");
  h2_mc_fakerate->GetYaxis()->SetBinLabel(2,"[35 - 45] ");
  h2_mc_fakerate->GetYaxis()->SetBinLabel(3,"[45 - 1000] ");
  //h2_mc_fakerate->GetYaxis()->SetBinLabel(4,"[60 - 100]");
  h2_mc_fakerate->Scale(100);
  h2_mc_fakerate->SetStats(0);
  h2_mc_fakerate->Draw("colztexterror");
  gStyle->SetPaintTextFormat("4.2f");
  h2_mc_fakerate->SaveAs(output_prefix+"h2_mc_FR.root");
  c14->SetLogy();
  c14 -> SaveAs(output_prefix+"h2_mc_fc.png");
  c14 -> SaveAs(output_prefix+"h2_mc_fc.svg");
  TH2F *h2_Scalefactor = new TH2F("SF","SF", NBINS_ETA, edges_eta, NBINS_PT, edges_pt);
  h2_Scalefactor->Sumw2();
  h2_Scalefactor->Divide(h2_fakerate,h2_mc_fakerate);
  TCanvas *c15= new TCanvas("c15","c15");
  h2_Scalefactor->SetTitle("2D fake rate scale factors");
  h2_Scalefactor->GetXaxis()->SetTitle("|eta|");
  h2_Scalefactor->GetYaxis()->SetTitle("pT [GeV]");
  h2_Scalefactor->GetXaxis()->SetBinLabel(1,"[0.0 - 0.6]");
  h2_Scalefactor->GetXaxis()->SetBinLabel(2,"[0.6 - 1.37]");
  h2_Scalefactor->GetXaxis()->SetBinLabel(3,"[1.37 - 1.52]");
  h2_Scalefactor->GetXaxis()->SetBinLabel(4,"[1.52 - 1.8]");
  h2_Scalefactor->GetXaxis()->SetBinLabel(5,"[1.8 - 2.37]");
  h2_Scalefactor->GetYaxis()->SetBinLabel(1,"[20 - 35] ");
  h2_Scalefactor->GetYaxis()->SetBinLabel(2,"[35 - 45] ");
  h2_Scalefactor->GetYaxis()->SetBinLabel(3,"[45 - 1000] ");
  //h2_Scalefactor->GetYaxis()->SetBinLabel(4,"[60 - 100]");
  h2_Scalefactor->Draw("colztexterror");
  c15->SetLogy();
  c15 -> SaveAs(output_prefix+"_sf.png");
  h2_Scalefactor->SaveAs(output_prefix+"_sf.root");
}

int main(){
  /// input files
  auto *f1 = new TFile("~/eos/physics_analysis/tty/efake_study/version_165/processed_ntuple_used_for_fitting/DataAnalysis_v7_different_binning_final.root");
  auto *f2 = new TFile("~/eos/physics_analysis/tty/efake_study/version_165/processed_ntuple_used_for_fitting/MC16Analysis_v7_nominal_different_binning.root");
  TH2F *h2_mc_fakerate = (TH2F*)f2->Get("hu2_mc_fcX"); // this is 2D MC fake rate.

  //output scale factor file
  output_prefix = "unconverted_syst_bkg"; // it will saved as output_prefix +.png +.root
  for(int EtaBin=0;EtaBin<5;EtaBin++){
    for(int PtBin=0;PtBin<3;PtBin++){
      if(EtaBin == 2) continue; // this histogram is empty
      TH1F *h1 = (TH1F*)f1->Get(Form("h_data_zee_eta%dpt%d",EtaBin+1,PtBin+1));
      Model model_zee(h1, low_range[PtBin], high_range[PtBin]);
      model_zee.setNL(1.0, 0, 10.0);
      model_zee.setNR(10.0, 0, 50.0);

      model_zee.setMean(90, 85, 95);
      model_zee.setSigma(1, 0, 5);
      model_zee.setMeanGaus(70, 60, 80);
      model_zee.setSigmaGaus(20, 0, 40);
			if((EtaBin == 0 && PtBin == 2) || (EtaBin==1 && PtBin==2)){
				model_zee.setMean(90, 85, 95);
    	  model_zee.setSigma(5, 0, 10);
    	  model_zee.setMeanGaus(70, 60, 80);
    	  model_zee.setSigmaGaus(20, 0, 40);
			}
      model_zee.isBkgGaus = true;
      model_zee.makeModel_DSCB_Gaus();
      fit(EtaBin, PtBin, model_zee, "zee");
	  }
  }
	// Zeg fit
  for(int EtaBin=0;EtaBin<5;EtaBin++){
    for(int PtBin=0;PtBin<3;PtBin++){
      if(EtaBin == 2) continue;
      TH1F *h1 = (TH1F *) f1->Get(Form("hu_data_zeg_eta%dpt%d", EtaBin + 1, PtBin + 1));
      TH1F *h2 = (TH1F *) f2->Get(Form("hu_mlpromptph_eta%dpt%d", EtaBin + 1, PtBin + 1));
      h1->Add(h2, -1);
      Model model_zeg(h1, low_range[PtBin], high_range[PtBin]);

      model_zeg.setMean(90, 80, 100);
      model_zeg.setSigma(5.0, 0, 10);
      //model_zeg.setNL(10.0, 0, 50.0);
      //model_zeg.setNR( 10.0, 0, 50.0);
      //model_zeg.setAlpaL(1, 0, 5);
      //model_zeg.setAlpaR(1, 0, 5);
      model_zeg.setMeanGaus(70, 60, 80);
      model_zeg.setSigmaGaus(20, 0, 40);
      if(EtaBin == 4 && PtBin == 0) model_zeg.m_invMassMin = 80;
      if((EtaBin == 2 && PtBin == 0) || (EtaBin == 4 && PtBin == 0)  ) {
        model_zeg.setMean(92, 85, 95);
        model_zeg.setSigma(2.0, 0, 3);
        model_zeg.setNL(10.0, 0, 50.0);
        model_zeg.setNR( 10.0, 0, 50.0);
        model_zeg.setAlpaL(1, 0, 5);
        model_zeg.setAlpaR(1, 0, 5);
      }
      else if(EtaBin == 3 && PtBin == 0){ // FIXME this fit needs to be made better
        model_zeg.m_invMassMin = 80;
        model_zeg.setMean(89, 80, 100);
        model_zeg.setSigma(8.0, 0, 20);

      }
      else if(EtaBin == 0 && PtBin == 0){
        //model_zeg.m_invMassMin = 80;
        model_zeg.setMean(90, 80, 100);
        model_zeg.setSigma(6.0, 0, 20);
        model_zeg.setAlpaL(1, 0, 10);
        model_zeg.setAlpaR(2, 0, 10);
      }
      else if((EtaBin == 0 && PtBin == 1) || (EtaBin == 0 && PtBin == 2)){
        //model_zeg.m_invMassMin = 80 ;
        model_zeg.setMean(90, 80, 100);
        model_zeg.setSigma(6.0, 0, 20);
        model_zeg.setAlpaL(1, 0, 10);
        model_zeg.setAlpaR(2, 0, 10);
        if(EtaBin == 0 && PtBin == 2){ //FIXME   this fit does not look good
          model_zeg.setMean(91, 80, 100);
          //model_zeg.setSigma(6.0, 5, 20);
          //model_zeg.setMeanGaus(90, 70, 100);
          //model_zeg.setSigmaGaus(30, 0, 60);
        }
      }
      model_zeg.isBkgGaus = true;
      model_zeg.makeModel_DSCB_Gaus();
      fit(EtaBin, PtBin, model_zeg, "zeg");
    }
  } // Zeg fit

   calculateFR_SF(h2_mc_fakerate);

	return 0;
}
