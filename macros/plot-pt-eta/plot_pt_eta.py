#!/usr/bin/env python3
import sys
import os
from ROOT import TFile, TCanvas, TLegend
#sys.path.append("/afs/cern.ch/work/b/bmondal/ttgamma-analysis/analysis_framework/source/") 
sys.path.append("/afs/cern.ch/user/b/bmondal/work/python_libraries/for_plotting")
from Plot import *

from ROOT import gROOT
atlasrootstyle_path = os.environ['HOME']+'/.local/opt/atlasrootstyle'
atlasrootstyle_path = '/afs/cern.ch/user/b/bmondal/work/atlasrootstyle'
gROOT.SetMacroPath(os.pathsep.join([gROOT.GetMacroPath(), atlasrootstyle_path ]))
gROOT.LoadMacro("AtlasLabels.C")
gROOT.LoadMacro("AtlasStyle.C")
gROOT.LoadMacro("AtlasUtils.C")

from ROOT import (
    gRandom,
    SetAtlasStyle,
    TH1F,
    ATLASLabel,
    myText
)
# ATLAS ROOT style
SetAtlasStyle()

mc_file_name='~/eos/physics_analysis/tty/efake_study/version_165/output-MC16Analysis_v7/MC16Analysis_v7_nominalLoose_different_binning.root'
data_file_name='~/eos/physics_analysis/tty/efake_study/version_165/output-MC16Analysis_v7/Data_v7_nominal_different_binning.root'

rebin=2
def plotKinDists(tag_probe_str, var, xaxisTitle, saveName): # tag_probe_str = "tag"/"probe", var = "pt"/"eta"
	h_mc_zee_probe_pt = Plot(mc_file_name, "h_mc_zee_"+tag_probe_str+"_"+var)
	h_mc_zee_probe_pt.setStyleMarker(6, 8)

	h_mc_zeg_probeA_pt = Plot(mc_file_name, "h_mc_zeg_"+tag_probe_str+"A_"+var)
	h_mc_zeg_probeA_pt.setStyleMarker(1,8)

	h_mc_zeg_probeB_pt = Plot(mc_file_name, "h_mc_zeg_"+tag_probe_str+"B_"+var)
	h_mc_zeg_probeB_pt.setStyleMarker(2,8)

	h_mc_zeg_probeC_pt = Plot(mc_file_name, "h_mc_zeg_"+tag_probe_str+"C_"+var)
	h_mc_zeg_probeC_pt.setStyleMarker(3,8)

	h_mc_zeg_probeD_pt = Plot(mc_file_name, "h_mc_zeg_"+tag_probe_str+"D_"+var)
	h_mc_zeg_probeD_pt.setStyleMarker(4,8)

	h_mc_zee_probe_pt.rebin(rebin)
	h_mc_zeg_probeA_pt.rebin(rebin)
	h_mc_zeg_probeB_pt.rebin(rebin)
	h_mc_zeg_probeC_pt.rebin(rebin)
	h_mc_zeg_probeD_pt.rebin(rebin)

	c = TCanvas('c')
	h_mc_zee_probe_pt.normalizeHist()
	h_mc_zeg_probeA_pt.normalizeHist()
	h_mc_zeg_probeB_pt.normalizeHist()
	h_mc_zeg_probeC_pt.normalizeHist()
	h_mc_zeg_probeD_pt.normalizeHist()

	h_mc_zee_probe_pt.setYaxisRange(0.1)
	h_mc_zee_probe_pt.draw("hist e")
	h_mc_zeg_probeA_pt.draw("hist same e")
	h_mc_zeg_probeB_pt.draw("hist same e")
	h_mc_zeg_probeC_pt.draw("hist same e")
	h_mc_zeg_probeD_pt.draw("hist same e")

	leg = TLegend(.68,.72,.93,.90)
	h_mc_zee_probe_pt.addToLegend(leg, "probe e")
	h_mc_zeg_probeA_pt.addToLegend(leg, "(a) mis-reco")
	h_mc_zeg_probeB_pt.addToLegend(leg, "(b) mis-match")
	h_mc_zeg_probeC_pt.addToLegend(leg, "(c) non-prompt QED")
	h_mc_zeg_probeD_pt.addToLegend(leg, "(d) prompt QED")
	leg.Draw()
	ATLASLabel(0.2,0.85,"Simulation")
	myText(   0.2,  0.80, 1, "#sqrt{s}= 13 TeV")
	myText(   0.2,  0.75, 1, "Ze#gamma/Zee MC")
	h_mc_zee_probe_pt.setXaxisTitle(xaxisTitle)
	h_mc_zee_probe_pt.setYaxisTitle("A.U")
	c.SaveAs(saveName)

def plotKinDists_mass(saveName): # tag_probe_str = "tag"/"probe", var = "pt"/"eta"
	h_mc_zee_probe_pt = Plot(mc_file_name, "h_mc_zee_m")
	h_mc_zee_probe_pt.setStyleMarker(6, 8)

	h_mc_zeg_probeA_pt = Plot(mc_file_name, "h_mc_zeg_mA")
	h_mc_zeg_probeA_pt.setStyleMarker(1,8)

	h_mc_zeg_probeB_pt = Plot(mc_file_name, "h_mc_zeg_mB")
	h_mc_zeg_probeB_pt.setStyleMarker(2,8)

	h_mc_zeg_probeC_pt = Plot(mc_file_name, "h_mc_zeg_mC")
	h_mc_zeg_probeC_pt.setStyleMarker(3,8)

	h_mc_zeg_probeD_pt = Plot(mc_file_name, "h_mc_zeg_mD")
	h_mc_zeg_probeD_pt.setStyleMarker(4,8)

	h_mc_zee_probe_pt.rebin(rebin)
	h_mc_zeg_probeA_pt.rebin(rebin)
	h_mc_zeg_probeB_pt.rebin(rebin)
	h_mc_zeg_probeC_pt.rebin(rebin)
	h_mc_zeg_probeD_pt.rebin(rebin)

	c = TCanvas('c')
	h_mc_zee_probe_pt.normalizeHist(1)
	h_mc_zeg_probeA_pt.normalizeHist(1)
	h_mc_zeg_probeB_pt.normalizeHist(1)
	h_mc_zeg_probeC_pt.normalizeHist(1)
	h_mc_zeg_probeD_pt.normalizeHist(1)

	h_mc_zee_probe_pt.setYaxisRange(0.2)
	h_mc_zee_probe_pt.draw("HIST e")
	h_mc_zeg_probeA_pt.draw("HIST SAME  e")
	h_mc_zeg_probeB_pt.draw("HIST SAME  e")
	h_mc_zeg_probeC_pt.draw("HIST SAME  e")
	h_mc_zeg_probeD_pt.draw("HIST SAME  e")

	leg = TLegend(.68,.72,.93,.90)
	h_mc_zee_probe_pt.addToLegend(leg, "probe e")
	h_mc_zeg_probeA_pt.addToLegend(leg, "m(tag,(a))")
	h_mc_zeg_probeB_pt.addToLegend(leg, "m(tag,(b))")
	h_mc_zeg_probeC_pt.addToLegend(leg, "m(tag,(c))")
	h_mc_zeg_probeD_pt.addToLegend(leg, "m(tag,(d))")
	leg.Draw()
	ATLASLabel(0.2,0.85,"Simulation")
	myText(   0.2,  0.80, 1, "#sqrt{s}= 13 TeV")
	myText(   0.2,  0.75, 1, "Ze#gamma/Zee MC")
	h_mc_zee_probe_pt.setXaxisTitle("m(#gamma,e) [GeV]")
	h_mc_zee_probe_pt.setYaxisTitle("A.U")
	c.SaveAs(saveName)

def plotStackMass(saveName): # tag_probe_str = "tag"/"probe", var = "pt"/"eta"

	h_mc_zeg_mA = Plot(mc_file_name, "h_mc_zeg_mA")
	#h_mc_zeg_mA.setStyleMarker(1,8)

	h_mc_zeg_mB = Plot(mc_file_name, "h_mc_zeg_mB")
	#h_mc_zeg_mB.setStyleMarker(2,8)

	h_mc_zeg_mC = Plot(mc_file_name, "h_mc_zeg_mC")
	#h_mc_zeg_mC.setStyleMarker(3,8)

	h_mc_zeg_mD = Plot(mc_file_name, "h_mc_zeg_mD")
	#h_mc_zeg_mD.setStyleMarker(4,8)

	hist_list=[h_mc_zeg_mC, h_mc_zeg_mD,h_mc_zeg_mB,h_mc_zeg_mA]
	h_stack = Plot(hist_list, True)

	c = TCanvas('c')
	h_stack.draw("hist PFC")

	leg = TLegend(.68,.72,.93,.90)
	h_mc_zeg_mA.addToLegend(leg, "m(tag,(a))")
	h_mc_zeg_mB.addToLegend(leg, "m(tag,(b))")
	h_mc_zeg_mC.addToLegend(leg, "m(tag,(c))")
	h_mc_zeg_mD.addToLegend(leg, "m(tag,(d))")
	leg.Draw()
	ATLASLabel(0.2,0.85,"Simulation")
	myText(   0.2,  0.80, 1, "#sqrt{s}= 13 TeV")
	myText(   0.2,  0.75, 1, "e-Fake")
	h_mc_zeg_mA.setXaxisTitle("m(#gamma,e) [GeV]")
	h_mc_zeg_mA.setYaxisTitle("Events")
	c.SaveAs(saveName)

def plot1DratePt(saveName): # tag_probe_str = "tag"/"probe", var = "pt"/"eta"
	h_mc_zeg_mA = Plot(mc_file_name, "h_mc_fcA_pt")
	h_mc_zeg_mA.rebin(20)
	h_mc_zeg_mA.normalizeHist(6.46)
	h_mc_zeg_mA.setStyleMarker(1,8)

	h_mc_zeg_mB = Plot(mc_file_name, "h_mc_fcB_pt")
	h_mc_zeg_mB.rebin(20)
	h_mc_zeg_mB.normalizeHist(0.13)
	h_mc_zeg_mB.setStyleMarker(2,8)

	h_mc_zeg_mC = Plot(mc_file_name, "h_mc_fcC_pt")
	h_mc_zeg_mC.rebin(20)
	h_mc_zeg_mC.normalizeHist(0.22)
	h_mc_zeg_mC.setStyleMarker(3,8)

	h_mc_zeg_mD = Plot(mc_file_name, "h_mc_fcD_pt")
	h_mc_zeg_mD.rebin(20)
	h_mc_zeg_mD.normalizeHist(0.58)
	h_mc_zeg_mD.setStyleMarker(4,8)

	#hist_list=[h_mc_zeg_mC, h_mc_zeg_mD,h_mc_zeg_mB,h_mc_zeg_mA]
	#h_stack = Plot(hist_list, True)

	c = TCanvas('c')
	#h_stack.draw("hist PFC")
	h_mc_zeg_mA.draw("HIST E");
	h_mc_zeg_mB.draw("HIST SAME E");
	h_mc_zeg_mC.draw("HIST SAME E");
	h_mc_zeg_mD.draw("HIST SAME E");

	leg = TLegend(.68,.72,.93,.90)
	h_mc_zeg_mA.addToLegend(leg, "FR(a)")
	h_mc_zeg_mB.addToLegend(leg, "FR(b)")
	h_mc_zeg_mC.addToLegend(leg, "FR(c)")
	h_mc_zeg_mD.addToLegend(leg, "FR(d)")
	leg.Draw()
	ATLASLabel(0.2,0.85,"Simulation")
	myText(   0.2,  0.80, 1, "#sqrt{s}= 13 TeV")
	myText(   0.2,  0.75, 1, "MC Fake Rate")
	h_mc_zeg_mA.setXaxisTitle(" p_{T} [GeV]")
	h_mc_zeg_mA.setYaxisTitle("Events")
	h_mc_zeg_mA.setYaxisRange(-0.05, 1.2)
	c.SaveAs(saveName)


def plot1DrateEta(saveName): # tag_probe_str = "tag"/"probe", var = "pt"/"eta"
	h_mc_zeg_mA = Plot(mc_file_name, "h_mc_fcA_eta")
	h_mc_zeg_mA.rebin(4)
	h_mc_zeg_mA.normalizeHist(6.46)
	h_mc_zeg_mA.setStyleMarker(1,8)

	h_mc_zeg_mB = Plot(mc_file_name, "h_mc_fcB_eta")
	h_mc_zeg_mB.rebin(4)
	h_mc_zeg_mB.normalizeHist(0.13)
	h_mc_zeg_mB.setStyleMarker(2,8)

	h_mc_zeg_mC = Plot(mc_file_name, "h_mc_fcC_eta")
	h_mc_zeg_mC.rebin(4)
	h_mc_zeg_mC.normalizeHist(0.22)
	h_mc_zeg_mC.setStyleMarker(3,8)

	h_mc_zeg_mD = Plot(mc_file_name, "h_mc_fcD_eta")
	h_mc_zeg_mD.rebin(4)
	h_mc_zeg_mD.normalizeHist(0.58)
	h_mc_zeg_mD.setStyleMarker(4,8)

	#hist_list=[h_mc_zeg_mC, h_mc_zeg_mD,h_mc_zeg_mB,h_mc_zeg_mA]
	#h_stack = Plot(hist_list, True)

	c = TCanvas('c')

	#h_stack.draw("hist PFC")
	h_mc_zeg_mA.draw("HIST E");
	h_mc_zeg_mB.draw("HIST SAME E");
	h_mc_zeg_mC.draw("HIST SAME E");
	h_mc_zeg_mD.draw("HIST SAME E");

	leg = TLegend(.48,.72,.73,.90)
	h_mc_zeg_mA.addToLegend(leg, "FR(a))")
	h_mc_zeg_mB.addToLegend(leg, "FR(b))")
	h_mc_zeg_mC.addToLegend(leg, "FR(c))")
	h_mc_zeg_mD.addToLegend(leg, "FR(d))")
	leg.Draw()
	ATLASLabel(0.2,0.85,"Simulation")
	myText(   0.2,  0.80, 1, "#sqrt{s}= 13 TeV")
	myText(   0.2,  0.75, 1, "MC Fake Rate")
	h_mc_zeg_mA.setXaxisTitle("#eta [GeV]")
	h_mc_zeg_mA.setYaxisTitle("Events")
	h_mc_zeg_mA.setYaxisRange(-0.05, 1.2)
        #c.SetLogy()
	c.SaveAs(saveName)


#def etaToAbsEta(hist):
#  hist_new = TH1F("abseta", "abs(eta)", 30, 0, 3.0);
#  for i in range(hist.GetNbinsX()):
#    hist_new.fill(TMath::Abs(hist.GetBinCenter(i)), hist.GetBinContent(i)) 
#  return hist_new
#for (Int_t i = 1; i <= Old_hist->GetNbinsX(); i++)
#  h1->Fill(TMath::Abs(Old_hist->GetBinCenter(i)), Old_hist->GetBinContent(i));
#h1->ResetStats(); // reset the statistics including the number of entries



if __name__ == '__main__':
	#plotKinDists("probe","pt", "probe p_{T} [GeV]", "probe_efake_pt.pdf")
	#plotKinDists("probe","eta", "probe #eta", "probe_efake_eta.pdf")
	#plotKinDists_mass("efake_invmass_comp.pdf")
	#plotStackMass("efake_invmass_stack.pdf")
	plot1DratePt("efake_rate_pt.pdf")
	plot1DrateEta("efake_rate_eta.pdf")
